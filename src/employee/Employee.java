import java.util.Date;

class Employee {
    String employeeId;
    String name;
    Date dataOfJoining;

    Employee(String employeeId , String name , Date dataOfJoining){
        this.employeeId = employeeId;
        this.name =  name;
        this.dataOfJoining = dataOfJoining;
    }

    public String getEmployeeId(){
        return employeeId;
    }

    public String getEmployeeName() {
        return name;
    }

    public Date getEmployeeDataOfJoining() {
        return dataOfJoining;
    }

    public boolean isPromotionDueThisYear(){
        return true;
    }

    public double calcIncomTaxForCurrentYear(){
        return 0.0;
    }
}

 class Demo{
    public static void main(String args[]){

    }
}

class Tool{
    public static void main(String args[]){

    }
}